import { DiscountDto } from './DiscountDto';

export interface VendorDto {
  id: string;
  image: string;
  title: string;
  type: string;
  rate: number;
  price: number;
  priceType: string;
  routeTimeOnFoot: string;
  routeTimeByCar: string;
  openingHours: string;
  categoryTags: string[];
  isBest: boolean;
  discounts: DiscountDto[];
}
