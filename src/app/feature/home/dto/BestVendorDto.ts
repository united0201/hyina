import { RestaurantDetails } from './RestaurantDetailsDto';

export class BestVendorDto {
  id: number;
  name: string;
  latPosition: number;
  longPosition: number;
  restaurantDetails: RestaurantDetails[];
  rating: number;
  allRestFoodTypes: string[];
  allPaymentTypes: string[];
  priceCategory: number[];
  hasOffers: boolean;
  favorite: boolean;
  walk?: string;
  drive?: string;

  constructor(vendor: any) {
    this.id = vendor.id;
    this.name = vendor.name;
    this.latPosition = vendor.lat_position;
    this.longPosition = vendor.long_position;
    this.restaurantDetails = vendor.restaurant_details;
    this.rating = this.calculateRating(vendor.restaurant_details);
    this.allRestFoodTypes = this.getAllRestFoodTypes(vendor.restaurant_details);
    this.allPaymentTypes = this.getAllPaymentTypes(vendor.restaurant_details);
    this.priceCategory = this.getAllPriceTypes(vendor.restaurant_details);
    this.isVendorHasOffers(vendor.restaurant_details);
    this.favorite = false;
  }

  private isVendorHasOffers(details: any[]) {
    this.hasOffers = false;
    details.map(d => {
      if (d.offers.length > 0) {
        this.hasOffers = true;
      }
    });
  }

  private getAllRestFoodTypes(details: any[]) {
    const types: string[] = [];
    details.forEach(provider => types.push(...provider.restaurant_type));
    return [...new Set(types)];
  }

  private getAllPriceTypes(details: any[]) {
    return [details[0].price_category];
  }

  private getAllPaymentTypes(details: any[]) {
    const paymentTypes: string[] = [];
    details.forEach(provider => {
          if (provider.payment_type) {
            paymentTypes.push(...provider.payment_type)
          }
        }
    );
    return [...new Set(paymentTypes)];
  }

  private calculateRating(details: any[]) {
    const calculated: number[] = [];
    const counts: number[] = [];

    details.forEach(r => {
      const rating = parseInt(r.rating_value, 5);
      const count = r.rating_count;
      const rCount = rating / 5;
      const rValue = rCount * count;
      counts.push(count);
      calculated.push(rValue);
    });

    const sumCalculated = calculated.reduce((previousValue, currentValue) => previousValue + currentValue, 0);
    const sumCount = counts.reduce((previousValue, currentValue) => previousValue + currentValue, 0);
    return Math.floor((sumCalculated / sumCount) * 100);
  }
}
