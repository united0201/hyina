import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component( {
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
} )

export class HeaderComponent implements OnInit {
    @Output() handleCountryChanged = new EventEmitter<FlagDto>( null );

    flags: FlagDto[] = [
        {
            url: 'assets/flags/flag01.svg',
            country: 'Dubai',
            index: 1
        },
        {
            url: 'assets/flags/flag02.svg',
            country: 'Malaisia',
            index: 2
        }
    ];
    selected: FlagDto;

    constructor() {
    }

    ngOnInit() {
        this.selected = this.flags[0];
        this.handleCountryChanged.emit( this.selected );
    }

    select( item: FlagDto ) {
        this.selected = item;
        this.handleCountryChanged.emit( this.selected );
    }

    isActive( item: FlagDto ) {
        return this.selected === item;
    }
}

export interface FlagDto {
    url: string;
    country: string;
    index: number;
}
