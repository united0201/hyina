import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { BestVendorDto } from '../../../dto/BestVendorDto';
import { RestaurantDetails } from '../../../dto/RestaurantDetailsDto';

@Component( {
    selector: 'app-vendor-card',
    templateUrl: './vendor-card.component.html',
    styleUrls: ['./vendor-card.component.scss']
} )
export class VendorCardComponent implements OnInit, OnChanges {
    @Input() vendor: BestVendorDto;
    details: RestaurantDetails;

    constructor() {
    }

    ngOnInit(): void {
    }

    ngOnChanges( changes: SimpleChanges ): void {
        const vendor = changes.vendor;
        if ( this.vendor && vendor && vendor.currentValue ) {
            this.vendor = vendor.currentValue;
            this.details = this.vendor.restaurantDetails.find( r => r.address );
        }
    }

    isNan( rating: number ) {
        return isNaN( rating );
    }

    getSubTitle( name: string ) {
        if ( name.length > 20 ) {
            return name.substring( 0, 20 ) + '...';
        } else {
            return name;
        }
    }
}
