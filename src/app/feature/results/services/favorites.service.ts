import { Injectable } from '@angular/core';

@Injectable()
export class FavoritesService {
    itemKey = 'favorites';

    addToFavorites( id: number ) {
        let items: number[] = this.getFavorites();

        if ( items ) {
            items.push( id );
            this.setFavorites( items );
        } else {
            items = [id];
            this.setFavorites( items );
        }
    }

    setFavorites( items: number[] ) {
        localStorage.setItem( this.itemKey, JSON.stringify( items ) );
    }

    getFavorites() {
        return JSON.parse( localStorage.getItem( this.itemKey ) ) ? JSON.parse( localStorage.getItem( this.itemKey ) ) : [];
    }

    removeFromFavorites( id: number ) {
        let items: number[] = this.getFavorites();
        if ( items ) {
            const index = items.findIndex( i => i === id );
            if ( index !== -1 ) {
                items.splice( index, 1 );
                this.setFavorites( items );
            }
        } else {
            items = [];
            this.setFavorites( items );
        }
    }

}
