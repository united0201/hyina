import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FilterService } from '../../services/filter.service';

@Component( {
    selector: 'app-search-form',
    templateUrl: './search-form.component.html',
    styleUrls: ['./search-form.component.scss']
} )
export class SearchFormComponent implements OnInit {

    searchForm: FormGroup;
    @Input() isMyBest: boolean;

    constructor(
        private fb: FormBuilder,
        private filterService: FilterService
    ) {
    }

    ngOnInit(): void {
        this.searchForm = this.fb.group( {
            searchInput: ['', []]
        } );
    }

    onSubmit() {
        if ( this.searchForm.invalid ) {
            return;
        }
        const newLine: string = this.searchForm.value.searchInput;
        this.filterService.updateSearchValue( newLine );
    }


}
