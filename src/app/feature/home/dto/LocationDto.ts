export interface LocationDto {
    city: string;
    country: string;
}
