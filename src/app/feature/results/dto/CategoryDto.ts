export interface CategoryDto {
  id: string;
  img: string;
  title: string;
}
