import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable( {
    providedIn: 'root'
} )
export class ApiService {

    constructor( private http: HttpClient ) {
    }

    getRestouraunts( lati: number, leti: number ) {
        return this.http.get( `${environment.apiUrl}/api/restaurants/?lat=${ lati }&long=${ leti }` );
    }

    getMenu( id: string ) {
        return this.http.get( `${environment.apiUrl}/api/restaurant_menu/${ id }/` );
    }

}
