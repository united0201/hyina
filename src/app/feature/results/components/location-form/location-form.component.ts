import { Component, OnInit } from '@angular/core';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { ActivatedRoute, Router } from '@angular/router';

@Component( {
    selector: 'app-location-form',
    templateUrl: './location-form.component.html',
    styleUrls: ['./location-form.component.scss']
} )
export class LocationFormComponent implements OnInit {
    region: string;
    // @ts-ignore
    options: {
        types: string[]; componentRestrictions: { country: string }
    } = {
        componentRestrictions: {country: ''}
    };

    constructor( private activeRoute: ActivatedRoute, private router: Router ) {
    }

    ngOnInit(): void {
        this.activeRoute.params.subscribe( params => {
            this.options.componentRestrictions.country = params.code;
            this.region = params.code;
        } );
    }

    onCountryChanged( $event: Address ) {
        const lat = $event.geometry.location.lat();
        const lete = $event.geometry.location.lng();
        this.router.navigate( ['results', this.region, lat, lete] );
    }

}
