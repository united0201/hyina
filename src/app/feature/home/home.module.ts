import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { SliderComponent } from './components/slider/slider.component';
import { BestVendorsComponent } from './components/best-vendors/best-vendors.component';
import { ApiService } from './api/api.service';
import { HomeComponent } from './home.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { VendorCardComponent } from './components/best-vendors/vendor-card/vendor-card.component';
import { FooterDubaiComponent } from './components/footers/footer-dubai/footer-dubai.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SearchFormMalaysiaComponent } from './components/search-forms/search-form-malaysia/search-form-malaysia.component';
import { SearchFormDubaiComponent } from './components/search-forms/search-form-dubai/search-form-dubai.component';
import { FooterMalaysiaComponent } from './components/footers/footer-malaysia/footer-malaysia.component';
import { HomeRoutingModule } from "./home.routing.module";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import {HttpClientModule} from "@angular/common/http";

@NgModule( {
    declarations: [
        HomeComponent,
        HeaderComponent,
        SliderComponent,
        BestVendorsComponent,
        VendorCardComponent,
        FooterDubaiComponent,
        SearchFormMalaysiaComponent,
        SearchFormDubaiComponent,
        FooterMalaysiaComponent
    ],
    imports: [
        CommonModule,
        CarouselModule,
        FormsModule,
        ReactiveFormsModule,
        HomeRoutingModule,
        GooglePlaceModule,
        HttpClientModule
    ],
    exports: [
        HomeComponent,
    ],
    providers: [
        ApiService
    ]
} )
export class HomeModule {
}
