import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BestVendorDto } from '../../../../home/dto/BestVendorDto';
import { CoorsDto } from '../vendors.component';

@Component( {
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss']
} )
export class CardComponent implements OnInit {

    @Input() vendor: BestVendorDto;
    @Input() coors: CoorsDto;
    @Output() addParentFavorites = new EventEmitter<number>();
    @Output() removeParentFavorites = new EventEmitter<number>();

    constructor() {
    }

    ngOnInit(): void {
    }

    addFavorites( $event: number ) {
        this.addParentFavorites.emit( $event );
    }

    removeFavorites( $event: number ) {
        this.removeParentFavorites.emit( $event );
    }
}
