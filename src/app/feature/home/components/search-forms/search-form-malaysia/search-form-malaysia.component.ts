import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { LocationDto } from '../../../dto/LocationDto';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { HttpClient } from '@angular/common/http';

@Component( {
    selector: 'app-search-form-malaysia',
    templateUrl: './search-form-malaysia.component.html',
    styleUrls: ['./search-form-malaysia.component.scss']
} )
export class SearchFormMalaysiaComponent implements OnInit, OnChanges {
    @Input() locations: LocationDto[];
    currentLocation: Address;

    // @ts-ignore
    options: { types: string[]; componentRestrictions: { country: string } } = {
        componentRestrictions: {country: 'MY'}
    };

    locationsToView: LocationDto[];
    popularLocations: string[] = ['Deira City Center', 'Academic City', 'Leeds'];
    locationForm: FormGroup;
    isFormOpen = false;

    constructor( private fb: FormBuilder, private router: Router, private http: HttpClient ) {
    }

    ngOnInit() {
        this.locationForm = this.fb.group( {
            location: [null, Validators.required],
        } );
    }

    onSubmit(): void {
        this.router.navigate( ['results', 'MY', this.currentLocation.geometry.location.lat(), this.currentLocation.geometry.location.lng()] );
        this.currentLocation = undefined;
    }


    ngOnChanges( changes: SimpleChanges ): void {
        const locations = changes.locations;
        if ( this.locations && locations && locations.currentValue ) {
            this.locations = locations.currentValue;
            this.locationsToView = locations.currentValue;
        }
    }

    handleLocationChoose( location: LocationDto ) {
        this.locationForm.get( 'location' ).setValue( location.city );
        this.isFormOpen = false;
    }

    handlePopularLocationChoose( pLocation: string ) {
        this.locationForm.get( 'location' ).setValue( pLocation );
        this.isFormOpen = false;
    }

    onCountryChanged( $event: Address ) {
        this.currentLocation = $event;
    }
}
