import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { BestVendorDto } from '../../../../../home/dto/BestVendorDto';
import { RestaurantDetails } from '../../../../../home/dto/RestaurantDetailsDto';
import { CoorsDto } from '../../vendors.component';
import { FavoritesService } from '../../../../services/favorites.service';

@Component( {
    selector: 'app-card-info',
    templateUrl: './card-info.component.html',
    styleUrls: ['./card-info.component.scss']
} )
export class CardInfoComponent implements OnInit, OnChanges {

    @Input() cardInfo: BestVendorDto;
    @Input() coors: CoorsDto;

    @Output() addFavorites = new EventEmitter<number>();
    @Output() removeFavorites = new EventEmitter<number>();

    cardDetails: RestaurantDetails;
    walking: string;
    driving: string;

    constructor( private favoriteService: FavoritesService ) {
    }

    ngOnInit(): void {
        if ( this.cardInfo ) {
            this.init();
        }
    }

    async init() {
        this.walking = await this.getWalk();
        this.driving = await this.getDrive();
    }

    getDrive(): Promise<string> {
        return new Promise<string>( resolve => {
            const f = new google.maps.LatLng( this.coors.leti, this.coors.long );
            const s = new google.maps.LatLng( this.cardInfo.latPosition, this.cardInfo.longPosition );
            const d = new google.maps.DistanceMatrixService();

            d.getDistanceMatrix( {
                origins: [f],
                destinations: [s],
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC
            }, (( response, status ) => {
                if ( status === 'OK' ) {
                    resolve( response.rows[0].elements[0].duration.text );
                } else {
                    console.error( status );
                }
            }) );
        } );
    }

    getWalk(): Promise<string> {
        return new Promise<string>( resolve => {
            const f = new google.maps.LatLng( this.coors.leti, this.coors.long );
            const s = new google.maps.LatLng( this.cardInfo.latPosition, this.cardInfo.longPosition );
            const d = new google.maps.DistanceMatrixService();

            d.getDistanceMatrix( {
                origins: [f],
                destinations: [s],
                travelMode: google.maps.TravelMode.WALKING,
                unitSystem: google.maps.UnitSystem.METRIC
            }, (( response, status ) => {
                if ( status === 'OK' ) {
                    resolve( response.rows[0].elements[0].duration.text );
                } else {
                    console.error( status );
                }
            }) );
        } );
    }

    ngOnChanges( changes: SimpleChanges ): void {
        const cardInfo = changes.cardInfo;
        if ( this.cardInfo && cardInfo && cardInfo.currentValue ) {
            this.cardInfo = cardInfo.currentValue;
            if ( this.cardInfo ) {
                this.cardDetails = this.cardInfo.restaurantDetails.find( r => r.address !== undefined );
            }
        }
    }

    isNan( rating: number ) {
        return isNaN( rating );
    }

    getPriceCategory( priceCategory: number ) {
        switch ( priceCategory ) {
            case 1:
                return '$';
            case 2:
                return '$$';
            case 3:
                return '$$$';
            case 4:
                return '$$$$';
            case 5:
                return '$$$$$';
        }
    }

    formatScheduler( timeScheduler: string ) {
        if ( timeScheduler.length > 30 ) {
            const first = timeScheduler.indexOf( ' ' );
            const subs = timeScheduler.substring( first + 1, 25 );
            return '( Mon - Sun ) ' + subs;
        } else {
            return timeScheduler;
        }
    }

    addToFavorites( id: number ) {
        const favorites: number[] = this.favoriteService.getFavorites();
        const index = favorites.findIndex( i => i === id );
        if ( index === -1 ) {
            this.favoriteService.addToFavorites( id );
            this.addFavorites.emit(id);
        } else {
            this.favoriteService.removeFromFavorites( id );
            this.removeFavorites.emit(id);
        }
    }
}
