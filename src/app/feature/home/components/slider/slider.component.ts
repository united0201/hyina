import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component( {
    selector: 'app-slider',
    templateUrl: './slider.component.html',
    styleUrls: ['./slider.component.sass']
} )
export class SliderComponent implements OnInit {
    customOptions: OwlOptions = {
        items: 1,
        loop: true,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false,
        dots: true,
        navSpeed: 700,
        nav: false,
        autoplay: true,
    };

    constructor() {
    }

    ngOnInit() {
    }

}
