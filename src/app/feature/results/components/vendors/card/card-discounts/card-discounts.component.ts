import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { BestVendorDto } from '../../../../../home/dto/BestVendorDto';
import { RestaurantDetails } from '../../../../../home/dto/RestaurantDetailsDto';

@Component( {
    selector: 'app-card-discounts',
    templateUrl: './card-discounts.component.html',
    styleUrls: ['./card-discounts.component.scss']
} )
export class CardDiscountsComponent implements OnInit, OnChanges {

    @Input() vendor: BestVendorDto;
    discounts: DiscountDto[];

    foodTypes: string[] = ['Foodpanda', 'Grabfood', 'Zoomato', 'Delivero'];

    constructor() {
    }

    ngOnInit(): void {
    }

    getDiscounts() {
        const discounts: DiscountDto[] = [];
        this.vendor.restaurantDetails.forEach( ( r, index ) => {
            if ( r.offers.length === 1 ) {
                discounts.push( {
                    id: r.id,
                    image: r.foodService.image,
                    title: r.offers[0],
                    url: r.url,
                    type: r.foodService.name,
                    discountCharge: r.deliveryCharge,
                    discountAmount: r.minOrderValue,
                    eta: r.eta,
                } );
            } else if ( r.offers.length > 1 ) {
                discounts.push( {
                    id: r.id,
                    image: r.foodService.image,
                    title: r.offers[0],
                    type: r.foodService.name,
                    url: r.url,
                    discountCharge: r.deliveryCharge,
                    discountAmount: r.minOrderValue,
                    eta: r.eta,
                    multiDiscounts: this.mapMultiDiscounts( r, r.offers )
                } );
            }
        } );

        return discounts;
    }

    private mapMultiDiscounts( rest: RestaurantDetails, offers: string[] ) {
        const multiOffers: DiscountDto[] = [];

        offers.map( ( offer, index ) => {
            multiOffers.push( {
                id: rest.id,
                image: rest.foodService.image,
                type: rest.foodService.name,
                url: rest.url,
                title: offer,
                discountCharge: rest.deliveryCharge,
                discountAmount: rest.minOrderValue,
                eta: rest.eta,
            } );
        } );

        return multiOffers;
    }

    // private detectTypeOfFood() {
    //     this.discounts.map( d => {
    //         const provider = this.foodTypes.findIndex( f => f === d.type );
    //         if ( provider !== -1 ) {
    //             this.foodTypes.splice( provider, 1 );
    //         }
    //     } );
    // }

    ngOnChanges( changes: SimpleChanges ): void {
        const vendor = changes.vendor;
        if ( this.vendor && vendor && vendor.currentValue ) {
            this.vendor = vendor.currentValue;
            this.discounts = this.getDiscounts();
            const vendorsWithoutOffers = this.vendor.restaurantDetails
                .filter( r => r.offers.length === 0 && this.foodTypes.find( f => f === r.foodService.name ) );

            if ( vendorsWithoutOffers.length > 0 ) {
                vendorsWithoutOffers.map( v => {
                    this.discounts.push( {
                        id: v.id,
                        image: v.foodService.image,
                        type: v.foodService.name,
                        url: v.url,
                        title: v.foodService.name + ': No Offers!',
                        discountCharge: v.deliveryCharge,
                        discountAmount: v.minOrderValue,
                        eta: v.eta,
                    } );
                } );
            }
            // this.detectTypeOfFood();
            // this.foodTypes.map( ( d ) => {
            //     if ( d !== 'Delivero' ) {
            //         this.discounts.push( {
            //             id: undefined,
            //             image: d,
            //             title: d,
            //             type: d,
            //             url: d,
            //             discountCharge: '2',
            //             discountAmount: null,
            //             discountTime: undefined,
            //             eta: 0,
            //         } );
            //     }
            // } );
        }
    }

}

export interface DiscountDto {
    id: string;
    image: string;
    type: string;
    title: string;
    url: string;
    discountAmount: number;
    discountCharge: string;
    eta: number;
    multiDiscounts?: DiscountDto[] | null;
}
