import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component( {
    selector: 'app-header-inner',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
} )
export class HeaderComponentInner implements OnInit {
    @Output() isMyBest = new EventEmitter<boolean>( null );
    myBest = false;

    constructor() {
    }

    ngOnInit(): void {
    }

    showMyBest() {
        this.myBest = !this.myBest;
        this.isMyBest.emit(this.myBest);
    }
}
