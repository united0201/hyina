import {Component, OnInit} from '@angular/core';
import {LocationDto} from "./dto/LocationDto";
import { FlagDto } from "./components/header/header.component";
import {ApiService} from "./api/api.service";

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    currentCountry: FlagDto;
    locations: LocationDto[] = [
        {
            city: 'Abu hail',
            country: 'Dubai'
        },
        {
            city: 'Academic City',
            country: 'Dubai'
        },
        {
            city: 'Academic City',
            country: 'Dubai'
        },
        {
            city: 'Abu hail',
            country: 'Dubai'
        },
    ]

    constructor(private api: ApiService) {
    }

    ngOnInit() {

    }

    countryChanges( $event: FlagDto ) {
        this.currentCountry = $event;
    }
}
