export interface DiscountDto {
  id: string;
  image: string;
  title: string;
  discountTime: string;
  discountAmount: number;
  discountMinimum: string;
  multiDiscounts?: DiscountDto[] | null;
  disable?: boolean;
}
