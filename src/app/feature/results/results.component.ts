import { Component, OnInit } from '@angular/core';

@Component( {
    selector: 'app-results',
    templateUrl: './results.component.html',
    styleUrls: ['./results.component.scss']
} )
export class ResultsComponent implements OnInit {
    isMyBest = false;

    constructor() {
    }

    ngOnInit(): void {

    }

    onMyBestShow( $event: boolean ) {
        this.isMyBest = !this.isMyBest;
    }
}
