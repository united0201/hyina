import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResultsRoutingModule } from './results.routing.module';
import { ResultsComponent } from './results.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { Ng5SliderModule } from 'ng5-slider';
import { HeaderComponentInner } from './components/header/header.component';
import { VendorsComponent } from './components/vendors/vendors.component';
import { CardComponent } from './components/vendors/card/card.component';
import { CardDiscountsComponent } from './components/vendors/card/card-discounts/card-discounts.component';
import { CardInfoComponent } from './components/vendors/card/card-info/card-info.component';
import { DiscountItemComponent } from './components/vendors/card/card-discounts/discount-item/discount-item.component';
import { FooterComponent } from './components/footer/footer.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { ApiService } from './api/api.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { FilterService } from './services/filter.service';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// @ts-ignore
import {  } from '@types/googlemaps';
import { NgxSmartModalModule, NgxSmartModalService } from 'ngx-smart-modal';
import { MenuComponent } from './containers/menu/menu.component';
import { LocationFormComponent } from './components/location-form/location-form.component';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { MenuSidebarComponent } from "./containers/menu/menu-sidebar/menu-sidebar.component";
import { MenuItemComponent } from "./containers/menu/menu-item/menu-item.component";
import { BrowserModule } from "@angular/platform-browser";
import { FavoritesService } from './services/favorites.service';


@NgModule( {
    declarations: [
        ResultsComponent,
        SidebarComponent,
        HeaderComponentInner,
        CategoriesComponent,
        VendorsComponent,
        CardComponent,
        CardDiscountsComponent,
        CardInfoComponent,
        DiscountItemComponent,
        FooterComponent,
        SearchFormComponent,
        MenuComponent,
        LocationFormComponent,
        MenuSidebarComponent,
        MenuItemComponent
    ],
    imports: [
        CommonModule,
        ResultsRoutingModule,
        CarouselModule,
        Ng5SliderModule,
        NgxPaginationModule,
        FormsModule,
        ReactiveFormsModule,
        GooglePlaceModule,
        NgxSmartModalModule.forRoot(),
    ],
    exports: [
        ResultsComponent
    ],
    providers: [
        ApiService,
        FilterService,
        NgxSmartModalService,
        FavoritesService
    ]
} )
export class ResultsModule {
}
