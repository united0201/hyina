import { AfterViewInit, Component, OnInit } from '@angular/core';
import { NgxSmartModalComponent, NgxSmartModalService } from 'ngx-smart-modal';
import { MenuDto } from "../../dto/MenuDto";

@Component( {
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
} )
export class MenuComponent implements OnInit, AfterViewInit {
    menuData: MenuDto[];
    currentCategoryId: number;
    restName: string;

    constructor( private ngxSmartModalService: NgxSmartModalService ) {
    }

    ngOnInit(): void {
    }

    categoryChanged( $event: number ) {
        this.currentCategoryId = $event;
    }

    ngAfterViewInit(): void {
        this.ngxSmartModalService.getModal( 'menu' ).onOpen.subscribe( ( modal: NgxSmartModalComponent ) => {
            this.menuData = modal.getData().value;
            this.restName = modal.getData().restName;
            if ( this.menuData.length > 0 ) {
                this.currentCategoryId = this.menuData[0].id;
            }
        } );
    }
}
