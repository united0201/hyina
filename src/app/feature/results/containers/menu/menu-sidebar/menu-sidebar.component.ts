import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { MenuDto } from '../../../dto/MenuDto';

@Component( {
    selector: 'app-menu-sidebar',
    templateUrl: './menu-sidebar.component.html',
    styleUrls: ['./menu-sidebar.component.scss']
} )
export class MenuSidebarComponent implements OnInit, OnChanges {

    @Input() menu: MenuDto[];

    @Output() onCategoryMenuChanged = new EventEmitter<number>( null );

    constructor() {
    }

    ngOnInit(): void {
    }

    ngOnChanges( changes: SimpleChanges ): void {
        const menu = changes.menu;
        if ( this.menu && menu && menu.currentValue ) {
            this.menu = menu.currentValue;
            this.changeMenuCategory( this.menu[0].id );
        }
    }

    changeMenuCategory( id: number ) {
        this.onCategoryMenuChanged.emit( id );
    }
}
