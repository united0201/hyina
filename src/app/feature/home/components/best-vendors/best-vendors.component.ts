import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {OwlOptions} from 'ngx-owl-carousel-o';
import {BestVendorDto} from '../../dto/BestVendorDto';
import {ApiService} from '../../api/api.service';
import {map, takeUntil} from 'rxjs/operators';
import {RestaurantDetails} from '../../dto/RestaurantDetailsDto';
import {Subject} from 'rxjs';
import {FlagDto} from '../header/header.component';

@Component({
  selector: 'app-best-vendors',
  templateUrl: './best-vendors.component.html',
  styleUrls: ['./best-vendors.component.scss']
})
export class BestVendorsComponent implements OnInit, OnDestroy, OnChanges {
  customOptions: OwlOptions = {
    items: 2,
    loop: false,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    nav: true,
    autoplay: false,
    // center: true,
    margin: 30,
    responsive: {
      400: {
        items: 1,
        slideBy: 1
      },
      510: {
        items: 2
      }
    }

    // autoHeight: false
  };
  topRestaurants: BestVendorDto[];

  @Input() currentLocation: FlagDto;

  destroy$ = new Subject<void>();

  constructor(private api: ApiService) {
  }

  ngOnInit() {

  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const currentLocation = changes.currentLocation;
    if (this.currentLocation && currentLocation && currentLocation.currentValue) {
      this.currentLocation = currentLocation.currentValue;
      this.api.getTorRestaurants(this.currentLocation.index)
          .pipe(
              takeUntil(this.destroy$),
              map((result: any) => {
                return result.map(detail => {
                  const details = new BestVendorDto(detail);
                  details.restaurantDetails = detail.restaurant_details.map(restaurant => {
                    return new RestaurantDetails(restaurant);
                  });
                  return details;
                });
              }),
          ).subscribe((value: any) => {
        this.topRestaurants = value;
      });
    }
  }

}
