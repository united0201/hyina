import {Component, OnInit} from '@angular/core';
import {LabelType, Options} from "ng5-slider";
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { FilterService } from '../../services/filter.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  iconOpen = '-';
  iconClose = '+';
  openedItems: string[] = [
    'price',
    'type',
    'payment',
    'distance',
  ];

  percent: number = 0;
  percentOptions: Options = {
    floor: 0,
    ceil: 100,
    step: 5,
  };

  discount: number = 0;
  discountOptions: Options = {
    floor: 0,
    ceil: 50,
    step: 5,
  };

  delivery: number = 0;
  deliveryOptions: Options = {
    floor: 0,
    step: 5,
    ceil: 100,
  };

  walk: number = 0;
  walkOptions: Options = {
    floor: 0,
    step: 5,
    ceil: 100
  };

  drive: number = 0;
  driveOptions: Options = {
    floor: 0,
    step: 5,
    ceil: 100
  };

  // priceMin: number = 1;
  priceMax: number = 0;
  priceOptions: Options = {
    floor: 0,
    step: 1,
    ceil: 4,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return '$' + '$'.repeat(value);
        case LabelType.High:
          return '$' + '$'.repeat(value);
        default:
          return '';
      }
    }
  };

  // Payment
  paymentForm: FormGroup;
  paymentTypes: any[];
  selectedPayments: string[] = [];

  //Price
  priceForm: FormGroup;
  priceTypes: any[];
  selectedPriceTypes: string[] = [];


  constructor(
      private fb: FormBuilder,
      private filterService: FilterService
  ) {
  }

  ngOnInit(): void {
    this.paymentForm = this.fb.group({
      paymentCash: ['', []],
      paymentCard: ['', []]
    });

    this.priceForm = this.fb.group({});

    this.paymentTypes = [
      { id: 1, type: 'Cash' },
      { id: 2, type: 'Online' }
    ];

    this.priceTypes = [
      { id: 1, priceType: '$' },
      { id: 2, priceType: '$$' },
      { id: 3, priceType: '$$$' },
      { id: 4, priceType: '$$$$' },
      { id: 5, priceType: '$$$$$' }
    ]



  }

  // Payment method
  filterByPayment(event) {
    const value = event.target.value.toLowerCase();
    const consist =  this.selectedPayments.includes(value);
    if(consist) {
      this.selectedPayments = this.selectedPayments.filter(el=> el!== value);
      this.filterService.updatePayments(this.selectedPayments);
    } else {
      this.selectedPayments.push(value);
      this.filterService.updatePayments(this.selectedPayments);
    }
  }

  // Offer method
  filterByOffers(event) {
    this.filterService.updateHasOffers(event.target.checked);
  }

  // PriceType Method
  filterByPrice(event) {
    const priceValue = event.target.value;
    const consist =  this.selectedPriceTypes.includes(priceValue);
    if(consist) {
      this.selectedPriceTypes = this.selectedPriceTypes.filter(el=> el!== priceValue);
      this.filterService.updatePriceTypes(this.selectedPriceTypes);
    } else {
      this.selectedPriceTypes.push(priceValue);
      this.filterService.updatePriceTypes(this.selectedPriceTypes);
    }
  }

  // Walk method
  filterByWalkTime(event) {
    this.filterService.updateWalkTime(event.value);
  }

  // Walk method
  filterByDriveTime(event) {
    this.filterService.updateDriveTime(event.value);
  }


  isContentOpen(type: string) {
    const index = this.openedItems.findIndex(i => i === type);
    (index !== -1) ? this.openedItems.splice(index, 1) : this.openedItems.push(type);
  }

  isContainsItem(type: string): boolean {
    return this.openedItems.findIndex(i => i === type) === -1;
  }
}
