export class MenuDto {
    id: number;
    name: string;
    foodItems: FoodItemDto[];

    constructor( menuItem: any ) {
        this.id = menuItem.id;
        this.name = menuItem.name;
        this.foodItems = menuItem.food_items;
    }
}

export interface FoodItemDto {
    id: number;
    name: string;
    price: number;
    image: string;
}
