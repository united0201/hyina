import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import { CategoryDto } from '../dto/CategoryDto';

@Injectable()
export class FilterService {

  // filter by Categories
  private categories = new BehaviorSubject<Array<CategoryDto>>([]);

  getCategories() {
    return this.categories.asObservable();
  }

  updateCategories(categories: CategoryDto[]) {
    this.categories.next(categories);
  }

  // filter by name
  private title = new BehaviorSubject<string>('');

  getSearchValue() {
    return this.title.asObservable();
  }

  updateSearchValue(newTitle: string) {
    this.title.next(newTitle);
  }

  // filter by Payment
  private payments = new BehaviorSubject<Array<string>>([]);

  getPayments() {
    return this.payments.asObservable();
  }

  updatePayments(payments: string[]) {
    this.payments.next(payments);
  }

  // filter by Offers
  private hasOffers = new BehaviorSubject<boolean>(false);

  getHasOffers() {
    return this.hasOffers.asObservable();
  }

  updateHasOffers(toggle: boolean) {
    this.hasOffers.next(toggle);
  }

  // filter by Price
  private priceTypes = new BehaviorSubject<Array<string>>([]);

  getPriceTypes() {
    return this.priceTypes.asObservable();
  }

  updatePriceTypes(priceTypes: string[]) {
    this.priceTypes.next(priceTypes);
  }

  // filter by Walk Time
  private walkTime = new BehaviorSubject<number>(0);

  getWalkTime() {
    return this.walkTime.asObservable();
  }

  updateWalkTime(newWalkTime: number) {
    this.walkTime.next(newWalkTime);
  }

  // filter by Drive Time
  private driveTime = new BehaviorSubject<number>(0);

  getDriveTime() {
    return this.driveTime.asObservable();
  }

  updateDriveTime(newDriveTime: number) {
    this.driveTime.next(newDriveTime);
  }

  constructor() { }

}
