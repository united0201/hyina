import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { BestVendorDto } from '../../../../../../home/dto/BestVendorDto';
import { DiscountDto } from '../card-discounts.component';
import { ApiService } from '../../../../../api/api.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { MenuDto } from "../../../../../dto/MenuDto";
import { map } from "rxjs/operators";

@Component( {
    selector: 'app-discount-item',
    templateUrl: './discount-item.component.html',
    styleUrls: ['./discount-item.component.scss']
} )
export class DiscountItemComponent implements OnInit, OnChanges {

    dropdownState = false;
    @Input() vendor: BestVendorDto;
    @Input() discountInfo: DiscountDto;
    restName: string;
    isLoading = false;

    constructor( private api: ApiService, public ngxSmartModalService: NgxSmartModalService ) {
    }

    ngOnInit(): void {
    }

    isOpen() {
        this.dropdownState = !this.dropdownState;
    }

    getImageUrl( type: string ) {
        switch ( type ) {
            case 'Foodpanda':
                return 'assets/offers/panda.png';
            case 'Grabfood':
                return 'assets/offers/grab.png';
            case 'Zoomato':
                return 'assets/offers/zomato.png';
            case 'Delivero':
                return 'assets/offers/delivero.jpg';
        }
    }

    openMenu() {
        this.isLoading = true;
        this.api.getMenu( this.discountInfo.id ).pipe(
            map( ( menu: any[] ) => {
                return menu.map( item => new MenuDto( item ) );
            } )
        ).subscribe( value => {
            this.ngxSmartModalService.getModal( 'menu' ).removeData();
            this.ngxSmartModalService.getModal( 'menu' ).setData( {value, restName: this.restName} );
            this.ngxSmartModalService.getModal( 'menu' ).open();
            this.isLoading = false;
        } );
    }

    ngOnChanges( changes: SimpleChanges ): void {
        const vendor = changes.vendor;
        if ( this.vendor && vendor && vendor.currentValue ) {
            this.vendor = vendor.currentValue;
            this.restName = this.vendor.name;
        }
    }
}
