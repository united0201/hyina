import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeModule } from './feature/home/home.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ResultsModule } from './feature/results/results.module';

@NgModule( {
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HomeModule,
        ResultsModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
} )
export class AppModule {
}
