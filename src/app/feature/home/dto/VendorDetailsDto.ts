export interface VendorDetailsDto {
  cash: number;
  hours: string;
  car: string;
  foot: string;
}
