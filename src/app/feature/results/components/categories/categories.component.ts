import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { CategoryDto } from '../../dto/CategoryDto';
import { FilterService } from '../../services/filter.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {


  selectedCategories: CategoryDto[] = [];

  categoriesArray: CategoryDto[] = [
    {
      id: '1',
      img: 'assets/categories/img_american.svg',
      title: 'American'
    },
    {
      id: '2',
      img: 'assets/categories/img_beverages.svg',
      title: 'Beverages'
    },
    {
      id: '3',
      img: 'assets/categories/img_chicken.svg',
      title: 'Chicken'
    },
    {
      id: '4',
      img: 'assets/categories/img_deserts.svg',
      title: 'Desserts'
    },
    {
      id: '5',
      img: 'assets/categories/img_sandwiches.svg',
      title: 'Sandwiches'
    },
    {
      id: '6',
      img: 'assets/categories/img_malaysian.svg',
      title: 'Malaysian food'
    },
    {
      id: '7',
      img: 'assets/categories/img_western.svg',
      title: 'Western'
    },
    {
      id: '8',
      img: 'assets/categories/img_asian.svg',
      title: 'Asian'
    },
    {
      id: '9',
      img: 'assets/categories/img_bubble-tea.svg',
      title: 'Bubble Tea'
    },
    {
      id: '10',
      img: 'assets/categories/img_seafood.svg',
      title: 'Seafood'
    },
    {
      id: '11',
      img: 'assets/categories/img_pizza.svg',
      title: 'Pizza'
    },
    {
      id: '12',
      img: 'assets/categories/img_burger.svg',
      title: 'Healthy Food'
    },
    {
      id: '13',
      img: 'assets/categories/img_healthy.svg',
      title: 'Burgers'
    },
    {
      id: '14',
      img: 'assets/categories/img_japanese.svg',
      title: 'Japanese'
    },

  ];

  customOptions: OwlOptions = {
    loop: false,
    mouseDrag: false,
    items: 7,
    slideBy: 7,
    touchDrag: true,
    pullDrag: false,
    dots: false,
    navSpeed: 300,
    margin: 0,
    nav: true,
    center: false,
    autoplay: false,
    navText: ['<img src=\'assets/categories/category-prev-arrow.svg\'>', '<img src=\'assets/categories/category-next-arrow.svg\'>'],
    responsive: {
      0: {
        items: 3,
        slideBy: 3
      },
      380: {
        items: 4,
        slideBy: 4
      },
      550: {
        items: 5,
        slideBy: 5
      },
      680: {
        items: 7,
        slideBy: 7
      }
    }
  };

  constructor(
      private filterService: FilterService
  ) {

  }

  ngOnInit(): void {
  }

  selectCategory(event, id) {
    const isCategorySelected = this.selectedCategories.find(category => category.id === id);
    if (isCategorySelected !== undefined) {
      this.selectedCategories = this.selectedCategories.filter(category => category.id !== id);
      event.currentTarget.classList.remove('selected');
      this.filterService.updateCategories(this.selectedCategories)

    } else {
      const selected = this.categoriesArray.find(el => el.id === id);
      this.selectedCategories = [...this.selectedCategories, selected];
      event.currentTarget.classList.add('selected');
      this.filterService.updateCategories(this.selectedCategories)
    }
  }
}
