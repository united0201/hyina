import { FoodService } from './FoodServiceDto';

export class RestaurantDetails {
    id: string;
    latPosition: number;
    longPosition: number;
    url: string;
    image: string;
    address: string;
    ratingValue: number;
    ratingCount: number;
    minOrderValue: number;
    deliveryCharge: string;
    timeScheduler: string;
    paymentType: string[];
    restaurantTypes: string[];
    foodService: FoodService;
    priceCategory: number;
    offers: string[];
    eta: number;

    constructor( details: any ) {
        this.id = details.id;
        this.latPosition = details.lat_position;
        this.longPosition = details.long_position;
        this.url = details.url;
        this.image = details.image;
        this.address = details.address;
        this.ratingValue = details.rating_value;
        this.ratingCount = details.rating_count;
        this.minOrderValue = details.min_order_value;
        this.deliveryCharge = details.delivery_charge;
        this.timeScheduler = details.time_scheduler;
        this.paymentType = details.payment_type;
        this.restaurantTypes = details.restaurant_type;
        this.foodService = details.food_service;
        this.offers = details.offers;
        this.priceCategory = details.price_category;
        this.eta = +details.eta;
    }
}
