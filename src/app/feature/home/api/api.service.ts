import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable( {
    providedIn: 'root'
} )
export class ApiService {
    constructor( private http: HttpClient ) {
    }

    getTorRestaurants( countryCode: number ) {
        return this.http.get( `${environment.apiUrl}/api/popular_restaurants/${ countryCode }/` );
    }

}
