import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { LocationDto } from '../../../dto/LocationDto';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

@Component( {
    selector: 'app-search-form-dubai',
    templateUrl: './search-form-dubai.component.html',
    styleUrls: ['./search-form-dubai.component.scss']
} )
export class SearchFormDubaiComponent implements OnInit, OnChanges {
    @Input() locations: LocationDto[];
    currentLocation: Address;

    locationsToView: LocationDto[];
    popularLocations: string[] = ['Deira City Center', 'Academic City', 'Leeds'];
    locationForm: FormGroup;
    isFormOpen = false;

    // @ts-ignore
    options: { types: string[]; componentRestrictions: { country: string } } = {
        componentRestrictions: {country: 'AE'}
    };

    constructor( private fb: FormBuilder, private router: Router ) {
    }

    ngOnInit() {
        this.locationForm = this.fb.group( {
            location: [null, Validators.required],
            cities: [null, Validators.required],
        } );
    }

    onCountryChanged( $event: Address ) {
        this.currentLocation = $event;
    }

    onSubmit(): void {
        this.router.navigate( ['results', 'AE', this.currentLocation.geometry.location.lat(), this.currentLocation.geometry.location.lng()] );
        this.currentLocation = undefined;
    }

    ngOnChanges( changes: SimpleChanges ): void {
        const locations = changes.locations;
        if ( this.locations && locations && locations.currentValue ) {
            this.locations = locations.currentValue;
            this.locationsToView = locations.currentValue;
        }
    }

    handleLocationChoose( location: LocationDto ) {
        this.locationForm.get( 'location' ).setValue( location.city );
        this.isFormOpen = false;
    }

    handlePopularLocationChoose( pLocation: string ) {
        this.locationForm.get( 'location' ).setValue( pLocation );
        this.isFormOpen = false;
    }

}
