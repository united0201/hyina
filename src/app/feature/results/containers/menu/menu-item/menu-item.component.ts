import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FoodItemDto, MenuDto } from "../../../dto/MenuDto";

@Component( {
    selector: 'app-menu-item',
    templateUrl: './menu-item.component.html',
    styleUrls: ['./menu-item.component.scss']
} )
export class MenuItemComponent implements OnInit, OnChanges {
    @Input() menu: MenuDto[];
    @Input() currentCategoryId: number;
    currentMenuItems: FoodItemDto[];
    p = 1;

    constructor() {
    }

    ngOnInit(): void {
    }

    getPage( $event: number ) {
        this.p = $event;
    }

    ngOnChanges( changes: SimpleChanges ): void {
        const menu = changes.menu;
        const currentCategoryId = changes.currentCategoryId;

        if ( this.menu && menu && menu.currentValue ) {
            this.menu = menu.currentValue;
        }

        if ( this.currentCategoryId && currentCategoryId && currentCategoryId.currentValue ) {
            this.currentCategoryId = currentCategoryId.currentValue;
            this.currentMenuItems = this.menu.find( m => m.id === this.currentCategoryId ).foodItems;
        }
    }

}
