import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./feature/home/home.module').then( m => m.HomeModule ),
    },
    {
        path: 'results/:code/:lat/:lete',
        loadChildren: () => import('./feature/results/results.module').then( m => m.ResultsModule ),
    },
];

@NgModule( {
    imports: [RouterModule.forRoot( routes )],
    exports: [RouterModule]
} )
export class AppRoutingModule {
}
