import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { BestVendorDto } from '../../../home/dto/BestVendorDto';
import { map, takeUntil, tap } from 'rxjs/operators';
import { RestaurantDetails } from '../../../home/dto/RestaurantDetailsDto';
import { ApiService } from '../../api/api.service';
import { FilterService } from '../../services/filter.service';
import { combineLatest, Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { FavoritesService} from '../../services/favorites.service';

@Component( {
    selector: 'app-vendors',
    templateUrl: './vendors.component.html',
    styleUrls: ['./vendors.component.scss']
} )
export class VendorsComponent implements OnInit, OnDestroy, OnChanges {
    p = 1;

    initRestaurants: BestVendorDto[];
    filteredRestaurants: BestVendorDto[];
    reservedRestaurants: BestVendorDto[];
    coors: CoorsDto;

    @Input() isMyBest: boolean;

    destroy$ = new Subject<void>();

    constructor(
        private api: ApiService,
        private filterService: FilterService,
        private activateRoute: ActivatedRoute,
        private favoriteService: FavoritesService
    ) {
    }

    ngOnInit(): void {
        this.activateRoute.params.subscribe( p => {
            this.coors = {
                leti: p.lat,
                long: p.lete
            };

            this.api.getRestouraunts( this.coors.leti, this.coors.long )
                .pipe(
                    takeUntil( this.destroy$ ),
                    map( ( result: any ) => {
                        return result.results.map( ( detail ) => {
                            const details = new BestVendorDto( detail );
                            details.restaurantDetails = detail.restaurant_details.map( restaurant => {
                                return new RestaurantDetails( restaurant );
                            } );
                            return details;
                        } );
                    } )
                ).subscribe( ( value: any ) => {
                const favorites = this.favoriteService.getFavorites();
                this.reservedRestaurants = this.mapFavorites( value, favorites ).slice();
                this.initRestaurants = this.reservedRestaurants.slice();
                this.filteredRestaurants = this.initRestaurants.slice();
            } );
        } );

        combineLatest( [
            this.filterService.getSearchValue(), // filter by name [0]
            this.filterService.getCategories(), // filter by categories [1]
            this.filterService.getPayments(), // filter by payment-type [2]
            this.filterService.getHasOffers(), // filter if offer consist [3]
            this.filterService.getPriceTypes(), // filter if price consist [4]
            this.filterService.getWalkTime(), // filter if walk time [5]
            this.filterService.getDriveTime() // filter if drive time [6]
        ] ).pipe(
            takeUntil( this.destroy$ )
        ).subscribe( filters => {
            this.p = 1;
            return this.filterItems( filters );
        } );
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    private mapFavorites( data: BestVendorDto[], favorites: number[] ): BestVendorDto[] {
        if ( favorites.length > 0 ) {
            data.map( vendor => {
                if ( favorites.find( f => f === vendor.id ) ) {
                    vendor.favorite = true;
                }
            } );
        }

        return data.slice();
    }

  filterItems(filters) {
    if (this.filteredRestaurants && this.initRestaurants) {
      const flatteredCategories = filters[1].map(item => item.title);
      this.filteredRestaurants = this.initRestaurants.slice()
          .filter(item => item.name.toLowerCase().includes(filters[0].toLowerCase())) //
          .filter(item => {
            if (flatteredCategories.length) {
              return item.allRestFoodTypes
                  .map(singleRestaurantCategory => flatteredCategories
                      .includes(singleRestaurantCategory)).some(item => item);
            }
            return item;
          })
          .filter(item => {
            if (filters[2] && filters[2].length) {
              return item.allPaymentTypes.map(singlePaymentType => filters[2].includes(singlePaymentType)).some(item => item);
            }
            return item;
          })
          .filter(item => filters[3] ? item.hasOffers === filters[3] : item)
          .filter(item => {
            if (filters[4].length) {
              return item.priceCategory.map(singleCategory => {
                if (singleCategory) {
                  return filters[4].includes(singleCategory.toString());
                }
              }).some(item => item);
            }
            return item;
          })
          .filter(item => {
            if (filters[5]) {
              return parseInt(item.walk) <= filters[5]
            }
            return item;
          })
          .filter(item => {
            if (filters[6] > 0) {
              return parseInt(item.drive) <= filters[6]
            }
            return item;
          })
    }


    }

    getPage( $event: number ) {
        this.p = $event;
    }

    addFavorites( $event: number ) {
        const index = this.initRestaurants.findIndex( v => v.id === $event );
        if ( index !== -1 ) {
            this.initRestaurants[index].favorite = true;
        }
    }

    ngOnChanges( changes: SimpleChanges ): void {
        const isMyBest = changes.isMyBest;
        if ( this.isMyBest !== undefined && isMyBest !== undefined && isMyBest.currentValue !== undefined ) {
            this.isMyBest = isMyBest.currentValue;

            if ( this.isMyBest ) {
                const items = this.favoriteService.getFavorites();
                this.p = 1;
                if ( items.length ) {
                    const result: BestVendorDto[] = [];
                    items.map( i => {
                        const rest = this.reservedRestaurants.find( r => r.id === i );
                        if ( rest ) {
                            result.push( rest );
                        }
                    } );
                    this.initRestaurants = result;
                    this.filteredRestaurants = this.initRestaurants.slice();
                } else {
                    this.initRestaurants = [];
                    this.filteredRestaurants = this.initRestaurants.slice();
                }
            } else {
                this.p = 1;
                if ( this.reservedRestaurants ) {
                    this.initRestaurants = this.reservedRestaurants.slice();
                    this.filteredRestaurants = this.initRestaurants.slice();
                }
            }
        }
    }

    removeFavorites( $event: number ) {
        const index = this.initRestaurants.findIndex( v => v.id === $event );
        if ( index !== -1 ) {
            this.initRestaurants[index].favorite = false;
            const items = this.favoriteService.getFavorites();
            if ( items.length ) {
                const result: BestVendorDto[] = [];
                items.map( i => {
                    const rest = this.initRestaurants.find( r => r.id === i );
                    if ( rest ) {
                        result.push( rest );
                    }
                } );
                this.initRestaurants = result;
                this.filteredRestaurants = this.initRestaurants.slice();
            }
        }
    }
}

export interface CoorsDto {
    leti: number;
    long: number;
}
